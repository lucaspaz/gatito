import React from "react";

import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import ServicesScreen from "./src/screens/services";
import CartScreen from "./src/screens/cart";
import { colors } from "./src/theme";

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName="Carrinho"
        tabBarOptions={{
          activeTintColor: colors.purple,
          inactiveTintColor: "white",
          allowFontScaling: false,
          showLabel: true,
          style: {
            height: 50,
            backgroundColor: colors.yellow,
            borderTopWidth: 0,
          },
          labelStyle: {
            fontWeight: 500,
            fontSize: 14,
          },
          tabStyle: {
            justifyContent: "center",
            alignItems: "center",
          },
        }}
      >
        <Tab.Screen name="Serviços" component={ServicesScreen} />
        <Tab.Screen name="Carrinho" component={CartScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
